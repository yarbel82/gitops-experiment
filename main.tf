terraform {
  backend "http" {}
  required_providers {
      amixr = {
        source = "alertmixer/amixr"
        version = "0.2.0"
      }
      digitalocean = {
        source = "digitalocean/digitalocean"
        version = "1.22.2"
      }
      //google = {
       // source = "hashicorp/google"
       // version = "3.38.0"
     // }
    }
}

// GCP SECTION
//variable "gcp_creds_file" {
//  type = string
//}

//provider "google" {
 //credentials = file(var.gcp_creds_file)
 //project     = "gitops-webinar"
 //region      = "us-central1-a"
//}

//resource "google_container_cluster" "default" {
//  name               = "my-cluster"
//  location           = "us-central1" # MUST BE A SINGLE ZONE, OTHERWISE IT COUNTS AS A REGIONAL CLUSTER
//
//  node_locations = ["us-central1-a", "us-central1-b", "us-central1-c"] # CAN BE MULTI ZONE
//
//  # We can't create a cluster with no node pool defined, but we want to only use
//  # separately managed node pools. So we create the smallest possible default
//  # node pool and immediately delete it.
//  remove_default_node_pool = true
//  initial_node_count       = 1
//}
//
//resource "google_container_node_pool" "preemptible_pool" {
//  name     = "preemptibe-node-pool"
//  cluster  = google_container_cluster.default.name
//  location   = "us-central1"
//
//  initial_node_count = 1
//  autoscaling {
//    min_node_count = 1
//    max_node_count = 3
//  }
//
//  management {
//    auto_repair = true
//  }
//
//  node_config {
//    machine_type = "g1-small"
//    preemptible  = true # Preemptible gives up to 80% discount
//
//    # Set the taints, can be used when you don'twant to schedule some containers here
//    taint {
//      key    = "role"
//      value  = "ops"
//      effect = "NO_SCHEDULE"
//    }
//  }
//}


// DO Section
variable "digitalocean_token" {
  type = string
}

provider "digitalocean" {
  token = var.digitalocean_token
}

//resource "digitalocean_kubernetes_cluster" "democluster" {
 // name    = "democluster"
 // region  = "fra1"
 // version = "1.18.6-do.0"
 // node_pool {
 //   name       = "worker-pool"
 //   size       = "s-1vcpu-2gb"
 //   node_count = 1



// Amixr Section
//variable "amixr_token" {
//  type = string
//}
//
//provider "amixr" {
//  token = var.amixr_token
//}
//


// Statuscake section
//provider "statuscake" {
//  username = "testuser"
//  apikey   = "12345ddfnakn"
//}
//
//resource "statuscake_test" "gitopswebinar" {
//  website_name = "gitopswebinar"
//  website_url  = "159.89.212.35"
//  test_type    = "HTTP"
//  check_rate   = 300
//  contact_group = ["amixr_gitops_webinar"]
//}